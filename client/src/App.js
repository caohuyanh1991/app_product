import Router from "./routers";
import Modal from 'react-modal';

function App() {
  return (
      <Router />
  )
}

// Make sure to bind modal to your appElement (https://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root');
export default App;
