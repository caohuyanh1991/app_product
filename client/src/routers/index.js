import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Navigation from "../components/pages/Navigation";
import HomePage from '../components/pages/HomePage'
import '../App.css'
import Category from "../components/pages/Category";
import Product from "../components/pages/Product";
import Cart from "../components/pages/Cart";
import Login from "../components/auth/Login"
import Register from "../components/auth/Register";
import Dashboard from "../components/pages/Dashboard";

export default function Routes() {
  return (
    <div>
      <Router>
        <Navigation />
        <Route exact path='/' component={HomePage} />
        <Route exact path='/category' component={Category} />
        <Route exact path='/product/:id' component={Product} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/register' component={Register} />
        <Route exact path='/cart' component={Cart} />
        <Route exact path='/dashboard' component={Dashboard} />
      </Router>
    </div>
  )
}
