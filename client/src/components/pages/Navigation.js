import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";
import CartHeader from "./CartHeader";
import { BsCart4 } from "react-icons/bs";
import CartModal from "./CartModal";
import { CartContext } from "../../contexts/cartContext";
const Navigation = (props) => {
  const { item } = props
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [cartClick, setCartClick] = useContext(CartContext);
  function setIsOpen() {
    setModalIsOpen(false)
  }
  const [productClick, setProductClick] = useState({})


  const handleClickProduct = (e) => {
    e.preventDefault()
    setModalIsOpen(true);
  }
  return (
    <nav>
      <Link to='/'>HOME</Link>
      <div className='cartlenght'>
        <nav>
          <Link to='/category'>CATEGORY</Link>
          <Link to='/login'>LOG IN</Link>

          <nav>
            <BsCart4 onClick={e => handleClickProduct(e, item)} />
            <CartModal modalIsOpen={modalIsOpen} setIsOpen={setIsOpen} cartClick={cartClick} />
            (<CartHeader />)
          </nav>

        </nav>
      </div>


    </nav>
  )
}

export default Navigation