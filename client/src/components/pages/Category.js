import axios from "axios";
import React, { useState, useEffect } from "react";
import ProductList from "./ProductsList";

export default function Category() {
  const [categorys, setCategorys] = useState([])
  const [category, setCategory] = useState([])
  const [productList, setProductList] = useState([])
  const [product, setProduct] = useState({});
  useEffect(() => {
    setProduct(productList[0]);
  }, [productList])
  useEffect(() => {
    getListCategorys();
  }, []);
  useEffect(() => {
    getListProducts();
  }, []);
  function handleClickCategory(e, item) {
    e.preventDefault()
    setCategory(item)
  }
  const getListCategorys = async () => {
    const res = await axios.get('http://localhost:5000/api/category')
    setCategorys(res.data.data)
  }
  const getListProducts = async () => {
    console.log(category,'111111111111');
    if(category._id){
      const res = await axios.get('http://localhost:5000/api/category/' + category._id)
      setProductList(res.data.data)
    }
  }
  console.log(productList);

  return (
    <>

      <div>
        <div className='category'>
          {
            categorys?.map((item, index) => (
              <li onClick={e => handleClickCategory(e, item)}>{item.name}</li>
            ))
          }

        </div>
        {
        productList?.map((item, index) => (
          <ProductList item={item} name="abc" key={index} />
        ))
      }
      </div>
    </>
  );
}