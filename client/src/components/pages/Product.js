import axios from "axios";
import { useParams } from "react-router-dom"
import React, { useState, useEffect } from "react";

export default function Product(props) {
  const {category } = props
  const [product, setProduct] = useState({});

  const { id } = useParams();
  // console.log(category, '11111111111111');
  // useEffect(() => {
  //   getProduct();
  // }, []);
  // const getProduct = async () => {
  //   const url = "http://localhost:5000/api/category/" + category._id
  //   const res = await axios.get(url)
  //   console.log(res, "2222222222222222222222222222")
  //   setProduct(res.data.data)
  // }
  return (
    <div className='product-list'>
      <img src={product.img} alt="" />
      <li>{product.name}</li>
      <li>{product.price}</li>
    </div>
  )
}
