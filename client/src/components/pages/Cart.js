import React, { useContext, useState } from "react";
import { CartContext } from "../../contexts/cartContext";
import Form from 'react-bootstrap/Form'
import axios from "axios";
export default function Cart() {
  const [cart, setCart] = useContext(CartContext);
  const [upCount, setUpCount] = useState(1)
  const [orderForm, setOrder] = useState({
    fullName: '',
    phone: '',
    address: ''
  })
  const { fullName, phone, address } = orderForm
  const totalPrice = cart.reduce((acc, item) => acc + item.item.price, 0);
  const onChangeOrderForm = event => setOrder({ ...orderForm, [event.target.name]: event.target.value })

  const newOrder = async event => {
    event.preventDefault()
    try {
      const orderData = await cart(orderForm)
    }
    catch (error) {
      console.log(error);
    }
  }
  function addCartItem() {
    setUpCount(upCount + 1)
  }
  function remoteCartItem() {
    setUpCount(upCount - 1)
  }
  function deleteCartItem(index) {
    const newCart = [...cart]
    newCart.splice(index, 1)
    setCart(newCart)
  };
  function countProduct(_id) {
    const countProduct = cart.filter((x) => x.item._id == _id)
  }
  const products = cart.map((i) => {
    i = {
      productId: i.item._id,
      productName: i.item.name,
      price: i.item.price

    }
    return i;
  })
  const bodyOrder = {
    fullName: orderForm.fullName,
    phone: orderForm.phone,
    address: orderForm.address,
    products: products
  }
  const userInfo = localStorage.getItem('userInfo')
  console.log(userInfo, 'userInfo');
  const createOrder = async () => {
    const res = await axios.post("http://localhost:5000/api/order", bodyOrder, {
      headers: {
        'Authorization': 'Bearer ' + userInfo
      }
    })
    setCart([])
  }
  return (
    <>
      <div className='cartlenght'>
        <Form onSubmit={newOrder}>
          <Form.Group className='my-3'>
            <Form.Control type='text'
              placeholder='fullName'
              name='fullName'
              require
              value={fullName}
              onChange={onChangeOrderForm}
            />
          </Form.Group>
          <Form.Group className='my-3'>
            <Form.Control type='phone'
              placeholder='phone'
              name='phone'
              require
              value={phone}
              onChange={onChangeOrderForm}
            />
          </Form.Group>
          <Form.Group className='my-3'>
            <Form.Control type='address'
              placeholder='address'
              name='address'
              require
              value={address}
              onChange={onChangeOrderForm}
            />
          </Form.Group>
          <li>items in cart: {cart.length}</li>
          <li>total price: {totalPrice} VND</li>
          <button onClick={createOrder}>Create Order</button>
        </Form>
      </div>
      {
        cart?.map((item, index) => (
          <div className='cart-item'>
            <p>{item.item.name}</p>
            <p>total price: {item.item.price}</p>
            <nav>
              <button onClick={() => addCartItem(item.item._id)}>+</button>
              <h2>{upCount}</h2>
              <button onClick={() => remoteCartItem(item.item._id)}>-</button>
              <button key={item.item._id} onClick={() => deleteCartItem(index)}>REMOTE ITEM</button>
            </nav>
          </div>
        ))
      }
    </>
  );
}