import React, { useState, useEffect, useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";
import { Button } from 'react-bootstrap';
import ProductModal from "./ProductModal";
import { CartContext } from "../../contexts/cartContext";

export default function ProductList(props) {
  const { item } = props
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [productClick, setProductClick] = useState({})
  const [cart, setCart] = useContext(CartContext);

   const cartHandler = (e, item) => {
    const product = { item };
    setCart((x) => [...x, product]);
  }
  const handleClickProduct = (e, item) => {
    e.preventDefault()
    setModalIsOpen(true);
    setProductClick(item);
  }

  function setIsOpen() {
    setModalIsOpen(false)
  }

  return (
    <>
    <div className="products">
      <div className="product-list">
        <div> 
          <img src={item.img} alt="" onClick={e => handleClickProduct(e, item)} />
          <li>{item.name}</li>
          <li>{item.price}</li>
        </div>
        <div>
          <Button  onClick={e => cartHandler(e, item)}>Add to Cart</Button>
        </div>
      </div>
      <ProductModal modalIsOpen={modalIsOpen} setIsOpen={setIsOpen} productClick={productClick} />
      </div>
    </>

  );
}

