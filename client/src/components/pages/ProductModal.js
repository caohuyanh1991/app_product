import React, { useContext } from "react";
import Modal from 'react-modal';
import { Button } from 'react-bootstrap';

export default function ProductModal(props) {
  const { modalIsOpen, setIsOpen, productClick}=  props;
  function closeModal() {
    setIsOpen();
  }

  return (
    <div >
      <Modal className='productModal'
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
      >
        <div>
        <img src={productClick.img} alt=""/>
          <li>{productClick.name}</li>
          <li>{productClick.price}</li>
        </div>
        <Button onClick={closeModal}>Close</Button> 
      </Modal>
    </div>
  );
}
