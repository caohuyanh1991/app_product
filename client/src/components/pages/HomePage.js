import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";
import setAuthToken from '../../setAuthToken'
import React, { useState, useEffect } from "react";
import ProductList from "./ProductsList";
import Cart from "./Cart";

export default function HomePage() {
  const [products, setProducts] = useState([]);
  const [product, setProduct] = useState({});
  console.log(product);

  useEffect(() => {
    getListProduct();
  }, [])

  useEffect(() => {
    setProduct(products[0]);
  }, [products])
  const userInfo = localStorage.getItem('userInfo')
  const getListProduct = async () => {
    const res = await axios.get("http://localhost:5000/api/product", {
      headers: {
        'Authorization': 'Bearer ' + userInfo
      }
    })
    setProducts(res.data.data)
  }

  function handleClick(item) {
    setProduct(item);
  }

  return (
    <> 
      {
        products?.map((item, index) => (

          <ProductList item={item} name="abc" key={index} onClickItem={handleClick} />
        ))
      }
      <div className='product'>
        <img src={product?.img} />
        <li>{product?.name}</li>
        <li>{product?.price}</li>
        <li>{product?.description}</li>
      </div>
    </>
  )
}