import React, { useContext, useState } from "react";
import Modal from 'react-modal';
import { Button } from 'react-bootstrap';
import { CartContext } from "../../contexts/cartContext";
import Form from 'react-bootstrap/Form'
import axios from "axios";
export default function CartModal(props) {

  const { modalIsOpen, setIsOpen, cartClick } = props;
  const [cart, setCart] = useContext(CartContext)
  const [orderForm, setOrder] = useState({
    fullName: '',
    phone: '',
    address: ''
  })
  const { fullName, phone, address } = orderForm
  const totalPrice = cart.reduce((acc, item) => acc + item.item.price, 0);
  const onChangeOrderForm = event => setOrder({ ...orderForm, [event.target.name]: event.target.value })

  const newOrder = async event => {
    event.preventDefault()
    try {
      const orderData = await cart(orderForm)
    }
    catch (error) {
      console.log(error);
    }
  }
  function closeModal() {
    setIsOpen();
  }
  function deleteCartItem(index) {
    const newCart = [...cart]
    newCart.splice(index, 1)
    setCart(newCart)
  };
  const products = cart.map((i) => {
    i = {
      productId: i.item._id,
      productName: i.item.name,
      price: i.item.price

    }
    return i;
  })
  const bodyOrder = {
    fullName: orderForm.fullName,
    phone: orderForm.phone,
    address: orderForm.address,
    products: products
  }
  const userInfo = localStorage.getItem('userInfo')
  const createOrder = async () => {
    const res = await axios.post("http://localhost:5000/api/order", bodyOrder, {
      headers: {
        'Authorization': 'Bearer ' + userInfo
      }
    })
    setCart([])
  }
  return (
    <Modal className='cartModal'
      isOpen={modalIsOpen}
      onRequestClose={closeModal}>
        <div className='cartlenght'>
          <Form onSubmit={newOrder}>
            <Form.Group className='my-3'>
              <Form.Control type='text'
                placeholder='fullName'
                name='fullName'
                require
                value={fullName}
                onChange={onChangeOrderForm}
              />
            </Form.Group>
            <Form.Group className='my-3'>
              <Form.Control type='phone'
                placeholder='phone'
                name='phone'
                require
                value={phone}
                onChange={onChangeOrderForm}
              />
            </Form.Group>
            <Form.Group className='my-3'>
              <Form.Control type='address'
                placeholder='address'
                name='address'
                require
                value={address}
                onChange={onChangeOrderForm}
              />
            </Form.Group>
            <li>items in cart: {cart.length}</li>
            <li>total price: {totalPrice} VND</li>
            <button onClick={createOrder}>Create Order</button>
          </Form>
        </div>
        {
        cart?.map((item, index) => (
          <div className='cart-item'>
            <p>{item.item.name}</p>
            <p>total price: {item.item.price}</p>
            <button key={item.item._id} onClick={() => deleteCartItem(index)}>REMOTE ITEM</button>
          </div>
        ))
      }
    </Modal>
  );
}
