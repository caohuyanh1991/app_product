import React, { useContext, useState, useEffect } from "react";
import { Form, Button } from 'react-bootstrap'
import axios from "axios";
import { Link } from "react-router-dom"
export default function Register() {
  const [registerForm, setRegisterForm] = useState({
    username: '',
    email: '',
    password: ''
  })
  const { email, username, password } = registerForm
  useEffect(() => {
    registerClick();
  }, [])
  const onChangeRegisterForm = event => setRegisterForm({ ...registerForm, [event.target.name]: event.target.value })
  const newUser = async event => {
    event.preventDefault()
  }
  const boydyUser = {
    email: registerForm.email,
    username: registerForm.username,
    password: registerForm.password
  }
  const registerClick = async () => {
    const res = await axios.post("http://localhost:5000/api/auth/register", boydyUser)
    setRegisterForm([])
  }
  return (
    <>
      <Form onSubmit={newUser}>
        <Form.Group className='my-3'>
          <Form.Control type='text'
            placeholder='Email'
            name='email'
            require
            value={email}
            onChange={onChangeRegisterForm}
          />
          <Form.Group className='my-3'>
            <Form.Control type='text'
              placeholder='Username'
              name='username'
              // require
              value={username}
              onChange={onChangeRegisterForm}
            />
          </Form.Group>
        </Form.Group>
        <Form.Group className='my-3'>
          <Form.Control type='password'
            placeholder='Password'
            name='password'
            require
            value={password}
            onChange={onChangeRegisterForm}
          />
        </Form.Group>
        <Link to='/login' >
        <Button type='submit' variant="success" onClick={registerClick}>Register</Button>
        </Link>
      </Form>
    </>
  )
}