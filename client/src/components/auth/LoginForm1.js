import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form'
import { Link } from "react-router-dom"
import { useState, useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";

const LoginForm = () => {
  
  const { loginUser } = useContext(AuthContext)
  const [loginForm, setLoginForm] = useState({
    username: '',
    password: ''
  })
  const { username, password } = loginForm
  const onChangeLoginForm = event => setLoginForm({ ...loginForm, [event.target.name]: event.target.value })
  const login = async event => {
    event.preventDefault()
    try {
      const loginData = await loginUser(loginForm)
    }
    catch (error) {
      console.log(error);
    }
  }
  return (
    <>
      <Form onSubmit={login}>
        <Form.Group className='my-3'>
          <Form.Control type='text'
            placeholder='Username'
            name='username'
            require
            value={username}
            onChange={onChangeLoginForm}
          />
        </Form.Group>
        <Form.Group className='my-3'>
          <Form.Control type='password'
            placeholder='Password'
            name='password'
            require
            value={password}
            onChange={onChangeLoginForm}
          />
        </Form.Group>
        <Button type='submit' variant="success">Login</Button>{' '}
      </Form>
      <p className='my-2'>Don't have an account?</p>
      <Link to='/register'>
        <Button variant='info' size='sm' className='m1-2'>Register</Button>
      </Link>
    </>
  )
}

export default LoginForm  