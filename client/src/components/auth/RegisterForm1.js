// import { Redirect } from "react-router-dom"
import { Form, Button } from "react-bootstrap"
import { Link } from "react-router-dom"
const RegisterForm = () => {
  return (
    <>
      <Form>
        <Form.Group className='my-3'>
          <Form.Control type='text'
            placeholder='Username'
            name='username'
            require />
        </Form.Group>
        <Form.Group className='my-3'>
          <Form.Control type='text'
            placeholder='Email'
            name='email' require
          />
        </Form.Group>
        <Form.Group className='my-3'>
          <Form.Control type='password'
            placeholder='Password'
            name='password'
            require
          />
        </Form.Group>
        <Form.Group className='my-3'>
          <Form.Control type='password'
            placeholder='Confirm Password'
            name='confirmpassword'
            require
          />
        </Form.Group>

      </Form>
      <Link to='/login'>
        <Button variant="success">Register</Button>{' '}
      </Link>
    </>
  )
}

export default RegisterForm