import React, { useContext, useState, useEffect } from "react";
import { Form, Button } from 'react-bootstrap'
import { LoginContext } from '../../contexts/loginContext'
import { Link } from "react-router-dom"
import axios from "axios";
export default function Login( {history }) {
  // const [login, setLogin] = useContext(LoginContext);
  const [loginForm, setLoginForm] = useState({
    username: '',
    password: ''
  })
  useEffect(() => {
    const userInfo = localStorage.getItem('userInfo')
    if (userInfo) {
      history.push("/dashboard");
    }
  }, []);
  const { username, password } = loginForm
  const onChangeLoginForm = event => setLoginForm({ ...loginForm, [event.target.name]: event.target.value })

  const boydyUser = {
    username: loginForm.username,
    password: loginForm.password
  }
  const loginClick = async () => {
    try {
      const res = await axios.post("http://localhost:5000/api/auth/login", boydyUser)
      localStorage.setItem('userInfo', res.data.accessToken)
      return res.data
    }
    catch (error) {
      if (error.res.data) return error.res.data
      else return {
        success: false,
        message: error.message
      }
    }
  }
  
  return (
    <>
      <Form >
        <Form.Group className='my-3'>
          <Form.Control type='text'
            placeholder='Username'
            name='username'
            require={username.toString()}
            value={username}
            onChange={onChangeLoginForm}
          />
        </Form.Group>
        <Form.Group className='my-3'>
          <Form.Control type='password'
            placeholder='Password'
            name='password'
            require={password.toString()}
            value={password}
            onChange={onChangeLoginForm}
          />
        </Form.Group>

            <Button type='submit' variant="success" onClick={loginClick}>Login</Button>
          <p>Bạn chưa có tài khoản</p>
          <Link to='/register' >
            <Button type='submit' variant="success">Register</Button>
          </Link>
      </Form>
    </>
  )
}