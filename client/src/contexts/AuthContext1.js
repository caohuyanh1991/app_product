import { createContext, useReducer, useState } from 'react';
import axios from 'axios'
import { apiUrl, LOCAL_STORAGE_TOKEN_NAME } from './constants'
import { authReducer } from '../redux/reducer/AuthReducer'

export const AuthContext = createContext()
const AuthContextProvider = ({ children }) => {
  const [authState, dispath] = useReducer(authReducer, {
    authLoading: true,
    isAuthenticated: false,
    user: null
  })
  
  const loginUser = async userForm => {
    try {
      const response = await axios.post(`${apiUrl}/auth/login`, userForm)
      if (response.data.success)
        localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, response.data.accessToken)
      return response.data
    }
    catch (err) {
      
      if (err.response.data) return err.response.data
      else return { success: false, message: err.message }
    }
  }
  const AuthContextData = { loginUser }
  return (
    <AuthContext.Provider value={AuthContextData}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContextProvider