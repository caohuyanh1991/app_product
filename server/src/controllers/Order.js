const mongoose = require('mongoose');
const Order = require('../models/Order');
const Product = require('../models/Product')
const verifyToken = require('../middleware/auth')
module.exports = {
  getAllOrder: async (req, res, next) => {
    try {
      const order = await Order.find()
      res.json(order)
    } catch (error) {
      console.log(error.message);
    }
  },

  createOrder: async (req, res) => {
    const { fullName, phone, address, products } = req.body;
    if (!fullName && !phone && !address && !products) {
      res.status(400).json({
        status: false,
        message: 'Invalid parameter'
      })
    }
    try {
      const newOrder = new Order({
        fullName,
        phone,
        address,
        products,
        user: req.userId
      })
      await newOrder.save()
      res.json({
        status: true,
        data: newOrder
      })
    }
    catch (err) {
      res.status(500).json({
        success: false,
        message: err.message
      })
    }
  }
}