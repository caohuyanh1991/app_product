const mongoose = require('mongoose');
const Product = require('../models/Product')
const Category = require('../models/Category');

module.exports = {
  getAllCategory: async (req, res) => {
    try {
      const category = await Category.find()
      res.json({
        success: true,
        data: category
      })
    } catch (error) {
      res.status(500).json({
        success: false,
        message: err.message
      })
    }
  },
  getOneCategory: async (req, res) => {
    const { id } = req.params
    try {
      const product = await Product.find({ categoryId: id })
      res.json({
        success: true,
        data: product
      })
    }
    catch (error) {
      res.status(500).json({
        success: false,
        message: err.message
      })
    }
  },

  createCategory: async (req, res) => {
    const { name, description } = req.body;
    if (!name) {
      res.status(400).json({
        success: false,
        message: 'Category name is not null'
      })
    }
    try {
      const newCategory = new Category({
        categoryName: name,
        description,
        userCreate: req.userId
      })
      await newCategory.save()
      res.json({
        success: true,
        message: 'create category is success',
        data: newCategory
      })
    }
    catch (error) {
      res.status(500).json({
        success: false,
        message: error.message
      })
    }
  }


}