const mongoose = require('mongoose');

const Post = require('../models/Post');

module.exports = {
  getAllPost: async (req, res, next) => {
    try {
      const post = await Post.find()
      res.json(post)
    } catch (error) {
      console.log(error.message);
    }
  }

}