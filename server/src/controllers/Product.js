const Product = require('../models/Product');

module.exports = {
  getAllProduct: async (req, res) => {
    try {
      const product = await Product.find()
      res.json({
        success: true,
        data: product
      })
    } catch (error) {
      res.status(500).json({
        success: false,
        message: err.message
      })
    }
  },

  getOneProduct: async (req, res) => {
    const { id } = req.params

    try {
      console.log(id);
      const product = await Product.findById(id)
      if (!product) {
        res.status(400).json({
          success: false,
          message: 'Product does not exist'
        })
      }
      res.json({
        success: true,
        data: product
      })
    } catch (error) {
      res.status(500).json({
        success: false,
        message: err.message
      })
    }
  },

  createProduct: async (req, res) => {
    const { categoryId, name, description, price, img } = req.body;
    console.log(name);
    if (!name) {
      res.status(400).json({
        status: false,
        message: 'Tên SP k đc để trống'
      })
    }
    try {
      const newProduct = new Product(
        { categoryId, name, description, price, img }
      )
      await newProduct.save()
      res.json({
        success: true,
        data: newProduct
      })
    }
    catch (err) {
      res.status(500).json({
        success: false,
        message: err.message
      })
    }
  }
}