const mongoose = require('mongoose');
const Schema = mongoose.Schema

const CategorySchema = new Schema({
    categoryName: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    img: {
        type: String
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    userCreate: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
})

module.exports = mongoose.model('category', CategorySchema);