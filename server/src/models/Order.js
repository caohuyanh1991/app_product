const mongoose = require('mongoose');
const Schema = mongoose.Schema

const OrderSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: Schema.Types.ObjectId,
            ref: 'product'
        },
        productName: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
    }],
    createAt: {
        type: Date,
        default: Date.now()
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
})

module.exports = mongoose.model('order', OrderSchema);