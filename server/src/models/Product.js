const mongoose = require('mongoose');
const Schema = mongoose.Schema

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
         require: true
    },
    amount: {
        type: Number,
        require: true
    },
    img: {
        type: String
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'category'
    },
    createAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('product', ProductSchema);