const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth')
const CategoryController = require('../controllers/Category');

//Get a list of all products
router.get('/', CategoryController.getAllCategory);
router.get('/:id', CategoryController.getOneCategory);
router.post('/', verifyToken, CategoryController.createCategory)

module.exports = router;