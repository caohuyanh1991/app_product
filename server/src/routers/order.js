const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth')
const OrderController = require('../controllers/order');

//Get a list of all products
router.get('/', OrderController.getAllOrder);

router.post('/', verifyToken, OrderController.createOrder);

module.exports = router;