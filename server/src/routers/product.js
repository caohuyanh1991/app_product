const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth')
const ProductController = require('../controllers/Product');

//Get a list of all products
router.get('/', ProductController.getAllProduct);
router.get('/:id', ProductController.getOneProduct);
router.post('/', ProductController.createProduct);

module.exports = router;