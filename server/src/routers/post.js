const express = require('express');
const router = express.Router()
const verifyToken = require('../middleware/auth')
const Post = require('../models/Post')
const postController = require('../controllers/post')

router.get('/', verifyToken, postController.getAllPost)

router.post('/', verifyToken, async (req, res, next) => {
  const { title, description, url, status } = req.body;
  if (!title)
    return res.status(400).json({
      success: false,
      message: 'Tiêu đề không được để trống'
    })
  try {
    const newPost = new Post({
      title,
      description,
      url,
      status: status || 'To Learn',
      user: req.userId
    })
    await newPost.save()
    res.json({
      success: true,
      message: 'Tạo post thành công',
      post: newPost
    })
  }
  catch (err) {
    res.status(500).json({
      success: false,
      message: 'Access token not found'
    })
  }
})

module.exports = router