const jwt = require('jsonwebtoken');
const verifyToken = (req, res, next) => {
  const authHeader = req.header('Authorization')
  const token = authHeader && authHeader.split(' ')[1]
  // const token = req.header('Authorization').replace('Bearer','');
  if (!token)
    return res.status(401).json({
      success: false,
      message: 'Access token not found'
    })
  try {
    const decode = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
    req.userId = decode.userID;
    next()
  }
  catch (errr) {
    res.status(403).json({
      success: false,
      message: 'Invalid token'
    })
  }
}

module.exports = verifyToken  