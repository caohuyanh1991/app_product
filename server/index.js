require('dotenv').config()
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const router = express.Router()
const cors = require('cors')
const http = require ('http')
const server = http.createServer(app)
const { Server} = require ('socket.io')

app.use(express.json())
app.use(cors())

mongoose.connect(process.env.MONGODB_URL || "", { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log("mongoose connected")
})

app.use('/api', router)

const authRouter = require('./src/routers/auth');
router.use('/auth', authRouter)
const postRouter = require('./src/routers/post')
router.use('/posts', postRouter)
const productRouter = require('./src/routers/product')
router.use('/product', productRouter)
const categoryRouter = require('./src/routers/category')
router.use('/category', categoryRouter)
const orderRouter = require('./src/routers/order')
router.use('/order', orderRouter)

const io = new Server(server, {
  cors: {
    origin: 'htpp://localhost:5000',
    methods: ['GET',  'POST']
  }
})
io.on('connect', (socket)=>{
  console.log(socket.id);
  socket.on('disconnect', ()=>{
    console.log('User Disconnect', socket.id);
  })
})


app.listen(process.env.PORT || 3000, () => {
  console.log('server is listening on port ' + process.env.PORT || 3000)
});

